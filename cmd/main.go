package main

import (
	"flag"
	"fmt"
	ilog "github.com/pion/ion-log"
	sdk "github.com/pion/ion-sdk-go"
	"github.com/pion/mediadevices"
	"github.com/pion/mediadevices/pkg/codec/x264"
	"github.com/pion/mediadevices/pkg/frame"
	"github.com/pion/mediadevices/pkg/prop"
	"github.com/pion/webrtc/v3"

	_ "github.com/pion/mediadevices/pkg/driver/screen" // This is required to register screen adapter
)

var (
	log = ilog.NewLoggerWithFields(ilog.DebugLevel, "main", nil)
)

func main() {
	// parse flag
	var session, addr string
	flag.StringVar(&addr, "addr", "localhost:5551", "ion-sfu grpc addr")
	flag.StringVar(&session, "session", "ion", "join session name")
	flag.Parse()

	connector := sdk.NewConnector(addr, sdk.ConnectorConfig{SSL: true})
	rtc := sdk.NewRTC(connector)

	rtc.GetPubTransport().GetPeerConnection().OnICEConnectionStateChange(func(state webrtc.ICEConnectionState) {
		log.Infof("Connection state changed: %s", state)
	})

	if err := rtc.Join(session, sdk.RandomKey(4)); err != nil {
		log.Errorf("join err=%v", err)
		panic(err)
	}

	h264Params, err := x264.NewParams()
	if err != nil {
		panic(err)
	}
	h264Params.Preset = x264.PresetUltrafast
	h264Params.BitRate = 8_000_000 // 8000kbps

	codecSelector := mediadevices.NewCodecSelector(
		mediadevices.WithVideoEncoders(&h264Params),
	)

	s, err := mediadevices.GetDisplayMedia(mediadevices.MediaStreamConstraints{
		Video: func(c *mediadevices.MediaTrackConstraints) {
			c.FrameFormat = prop.FrameFormat(frame.FormatRGBA)
			c.Width = prop.Int(1920)
			c.Height = prop.Int(1080)
			c.FrameRate = prop.Float(60)
		},
		Codec: codecSelector,
	})

	if err != nil {
		panic(err)
	}

	for _, track := range s.GetTracks() {
		track.OnEnded(func(err error) {
			fmt.Printf("Track (ID: %s) ended with error: %v\n",
				track.ID(), err)
		})
		_, err = rtc.Publish(track)
		if err != nil {
			panic(err)
		} else {
			break // only publish first track, thanks
		}
	}
	select {}
}
