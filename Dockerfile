FROM ubuntu:21.10 as builder

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    golang \
    pkg-config \
    libx264-dev \
    libvpx-dev \
    wget \
    curl \
    gcc libxext-dev libx11-dev gstreamer1.0-libav

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download
COPY . .

RUN go build -o ./webrtc-streamer cmd/main.go


FROM ubuntu:21.10
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y gcc libx264-dev libvpx-dev libxext-dev libx11-dev curl wget

COPY --from=builder /app/webrtc-streamer /app

ENTRYPOINT ["./app"]

CMD ["-addr", "localhost:9000", "-session", "ion"]

